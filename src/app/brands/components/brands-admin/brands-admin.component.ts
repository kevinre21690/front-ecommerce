import {Component, OnInit, Input, Output, ElementRef, ViewChild} from '@angular/core';
import {UntypedFormBuilder, Validators, ValidationErrors, FormControl, FormGroup} from '@angular/forms';
import {NgbActiveModal, NgbModal, NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';
import {BrandsService} from "@core/services/brands/brands.service";
import {Router, NavigationEnd} from "@angular/router";
import {SharedService} from "@core/services/shared/shared.service";
import {HttpClient} from '@angular/common/http';
import Swal from "sweetalert2";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-brands-admin',
  templateUrl: './brands-admin.component.html',
  styleUrls: ['./brands-admin.component.css']
})
export class BrandsAdminComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal,
              private formBuilder: UntypedFormBuilder,
              private router: Router,
              private sharedService: SharedService,
              private readonly brandsService: BrandsService,
              private modalService: NgbModal) {
  }

  @Input() public header: any;
  @Input() public content: any;
  btnActive: boolean = false;

  formBrand = this.formBuilder.group({
    id: [''],
    name: ['',
      Validators.compose([
        Validators.required,
        Validators.pattern(new RegExp('^[A-Za-z ]+$')),
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
    reference: ['',
      Validators.compose([
        Validators.required,
        Validators.pattern(new RegExp('^[0-9a-zA-Z]+$')),
        Validators.minLength(2),
        Validators.maxLength(15),
      ])],
  });

  ngOnInit() {
    if (this.content.opc === 'edit') {
      this.btnActive = true;
      const data = this.content.fields[0];
      if (data === undefined || data === "") {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        this.router.navigate(['/home', {}]);
        return;
      }

      this.formBrand.get('id')?.setValue(data.id);
      this.formBrand.get('name')?.setValue(data.nombre);
      this.formBrand.get('reference')?.setValue(data.referencia);
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modalService.dismissAll();
      }
    });
  }

  close(): void {
    this.modalService.dismissAll();
    //window.location.reload();
  }

  editBrand(): void {
    const dataObj: any = {
      nombre: this.formBrand.get('name')?.value,
      referencia: this.formBrand.get('reference')?.value
    }
    this.sharedService.showLoad();
    this.brandsService.editBrand(this.formBrand.get('id')?.value, dataObj).pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      } else if (response.status_code === 200){
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        }).then((result) => {
          if (result.isConfirmed) {
            window.location.reload();
          }
        });
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  saveBrand(): void {
    const dataObj: any = {
      nombre: this.formBrand.get('name')?.value,
      referencia: this.formBrand.get('reference')?.value
    }
    this.sharedService.showLoad();
    this.brandsService.saveBrand(dataObj).pipe(first()).subscribe((data: any) => {
      const response: any = data;
      if (typeof (response.status_code) === 'undefined' || response.status_code === 500) {
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        });
        this.router.navigate(['/home', {}]);
        return;
      } else if (response.status_code === 400) {
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        });
        this.router.navigate(['/home', {}]);
        return;
      } else if (response.status_code === 201) {
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        }).then((result) => {
          if (result.isConfirmed) {
            window.location.reload();
          }
        });
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
      });
      this.router.navigate(['/home', {}]);
      return;
    });
  }
}
