import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandsAdminComponent } from './brands-admin.component';

describe('BrandsAdminComponent', () => {
  let component: BrandsAdminComponent;
  let fixture: ComponentFixture<BrandsAdminComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BrandsAdminComponent]
    });
    fixture = TestBed.createComponent(BrandsAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
