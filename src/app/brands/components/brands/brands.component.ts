import {Component, OnInit} from '@angular/core';
import {SharedService} from "@core/services/shared/shared.service";
import {BrandsService} from "@core/services/brands/brands.service";
import {BrandsAdminComponent} from "../brands-admin/brands-admin.component";
import {NgbNavConfig, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {first} from 'rxjs';
import Swal from "sweetalert2";

@Component({
  selector: 'app-brands',
  templateUrl: './brands.component.html',
  styleUrls: ['./brands.component.css']
})
export class BrandsComponent implements OnInit {

  listBrads: any[] = [];
  showdata: boolean = false;

  constructor(private sharedService: SharedService,
              private readonly brandsService: BrandsService,
              private modalService: NgbModal,
              private router: Router) {
  }

  ngOnInit() {
    this.getBrands();
  }

  getBrands(): void {
    this.sharedService.showLoad();
    this.brandsService.getBrands().pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      } else if (response.status_code === 200) {
        this.listBrads = response.data;
        console.log(this.listBrads)
        if (response.data.length !== 0) {
          this.showdata = true;
        }
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  deleteBrand(id: number): void {
    this.sharedService.showLoad();
    this.brandsService.deleteBrand(id).pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      }else if (response.status_code === 200){
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        }).then((result) => {
          if (result.isConfirmed) {
            this.getBrands();
          }
        });
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  getBrandId(id: number): void {
    this.sharedService.showLoad();
    this.brandsService.getBrandId(id).pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      }else if (response.status_code === 200) {
        this.sharedService.hideLoad();
        this.modalBrands('edit', response.data);
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  modalBrands(option: string, data: any): void {
    const modalRef = this.modalService.open(BrandsAdminComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.header = 'Marcas admin';
    modalRef.componentInstance.content = { opc: option, fields: data};
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }
}
