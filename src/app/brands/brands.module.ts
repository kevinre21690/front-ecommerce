import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrandsRoutingModule} from './brands-routing.module';
import {BrandsComponent} from './components/brands/brands.component';
import { BrandsAdminComponent } from './components/brands-admin/brands-admin.component';
import { ReactiveFormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    BrandsComponent,
    BrandsAdminComponent
  ],
  imports: [
    CommonModule,
    BrandsRoutingModule,
    ReactiveFormsModule
  ]
})
export class BrandsModule {
}
