export interface Response{
  code: number;
  msgResponse: string;
  type: string;
  data: [];
  failMessage: string;
}
