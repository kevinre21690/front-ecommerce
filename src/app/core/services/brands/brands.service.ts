import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from "@environments//environment";
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BrandsService {

  static handleError(error: HttpErrorResponse): any {
    return throwError('Ha ocurrido un error.');
  }
  constructor(private http: HttpClient) {
  }

  getBrands(): any {
    return this.http.get<Response>(`${environment.url_api}/marca`).pipe(
      catchError(BrandsService.handleError));
  }

  saveBrand(data: object): any {
    return this.http.post<Response>(`${environment.url_api}/marca`, data).pipe(
      catchError(BrandsService.handleError));
  }

  deleteBrand(idMarca: number): any {
    const id: number = idMarca;
    return this.http.delete<Response>(`${environment.url_api}/marca/${id}`).pipe(
      catchError(BrandsService.handleError));
  }

  getBrandId(idMarca: number): any {
    const id: number = idMarca;
    return this.http.get<Response>(`${environment.url_api}/marca/${id}`).pipe(
      catchError(BrandsService.handleError));
  }

  editBrand(id: number, data: object): any {
    return this.http.put<Response>(`${environment.url_api}/marca/${id}`, data).pipe(
      catchError(BrandsService.handleError));
  }
}
