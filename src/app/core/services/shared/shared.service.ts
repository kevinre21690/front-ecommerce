import {Injectable} from '@angular/core';
import {throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {State} from '@core/models/State';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private http: HttpClient,
              private store: Store<{ load: State }>) {
  }

  static handleError(error: HttpErrorResponse): any {
    return throwError('Ha ocurrido un error.');
  }

  showLoad(): void {
    const x = document.querySelectorAll<HTMLElement>('#loader-wrapper')[0];
    x.style.display = 'block';
  }

  hideLoad(): void {
    const x = document.querySelectorAll<HTMLElement>('#loader-wrapper')[0];
    x.style.display = 'none';
  }
}
