import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from "@environments//environment";
import {throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  static handleError(error: HttpErrorResponse): any {
    return throwError('Ha ocurrido un error.');
  }
  constructor(private http: HttpClient) {
  }

  getProducts(): any {
    return this.http.get<Response>(`${environment.url_api}/producto`).pipe(
      catchError(ProductsService.handleError));
  }

  getTallas(): any {
    return this.http.get<Response>(`${environment.url_api}/talla`).pipe(
      catchError(ProductsService.handleError));
  }

  saveProduct(data: object): any {
    return this.http.post<Response>(`${environment.url_api}/producto`, data).pipe(
      catchError(ProductsService.handleError));
  }

  deleteProduct(idMarca: number): any {
    const id: number = idMarca;
    return this.http.delete<Response>(`${environment.url_api}/producto/${id}`).pipe(
      catchError(ProductsService.handleError));
  }

  getProductId(idMarca: number): any {
    const id: number = idMarca;
    return this.http.get<Response>(`${environment.url_api}/producto/${id}`).pipe(
      catchError(ProductsService.handleError));
  }

  editProduct(id: number, data: object): any {
    return this.http.put<Response>(`${environment.url_api}/producto/${id}`, data).pipe(
      catchError(ProductsService.handleError));
  }
}

