import {Component, OnInit} from '@angular/core';
import {SharedService} from "@core/services/shared/shared.service";
import {ProductsService} from "@core/services/products/products.service";
import {ProductsAdminComponent} from "../products-admin/products-admin.component";
import {NgbNavConfig, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {first} from 'rxjs';
import Swal from "sweetalert2";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})

export class ProductsComponent implements OnInit {

  listProducts: any[] = [];
  showdata: boolean = false;

  constructor(private sharedService: SharedService,
              private readonly productsService: ProductsService,
              private modalService: NgbModal,
              private router: Router) {
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts(): void {
    this.sharedService.showLoad();
    this.productsService.getProducts().pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      } else if (response.status_code === 200) {
        this.listProducts = response.data;
        if (response.data.length !== 0) {
          this.showdata = true;
        }
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  deleteProduct(id: number): void {
    this.sharedService.showLoad();
    this.productsService.deleteProduct(id).pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      }else if (response.status_code === 200){
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        }).then((result) => {
          if (result.isConfirmed) {
            this.getProducts();
          }
        });
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  getProductId(id: number): void {
    this.sharedService.showLoad();
    this.productsService.getProductId(id).pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      }else if (response.status_code === 200) {
        this.sharedService.hideLoad();
        this.modalProduct('edit', response.data);
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  modalProduct(option: string, data: any): void {
    const modalRef = this.modalService.open(ProductsAdminComponent, { size: 'xl', backdrop: 'static' });
    modalRef.componentInstance.header = 'Productos admin';
    modalRef.componentInstance.content = { opc: option, fields: data};
    modalRef.result.then((result) => {
    }, (reason) => {
    });
  }
}

