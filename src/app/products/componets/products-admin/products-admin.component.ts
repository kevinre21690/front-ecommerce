import {Component, OnInit, Input, Output, ElementRef, ViewChild} from '@angular/core';
import {UntypedFormBuilder, Validators, ValidationErrors, FormControl, FormGroup} from '@angular/forms';
import {NgbActiveModal, NgbModal, NgbTypeaheadSelectItemEvent} from '@ng-bootstrap/ng-bootstrap';
import {ProductsService} from "@core/services/products/products.service";
import {BrandsService} from "@core/services/brands/brands.service";
import {Router, NavigationEnd} from "@angular/router";
import {SharedService} from "@core/services/shared/shared.service";
import {HttpClient} from '@angular/common/http';
import Swal from "sweetalert2";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-products-admin',
  templateUrl: './products-admin.component.html',
  styleUrls: ['./products-admin.component.css']
})
export class ProductsAdminComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal,
              private formBuilder: UntypedFormBuilder,
              private router: Router,
              private sharedService: SharedService,
              private readonly productsService: ProductsService,
              private readonly brandsService: BrandsService,
              private modalService: NgbModal) {
  }

  @Input() public header: any;
  @Input() public content: any;
  btnActive: boolean = false;
  listBrads: any[] = [];
  listTallas: any[] = [];

  formProducts = this.formBuilder.group({
    id: [''],
    name: ['',
      Validators.compose([
        Validators.required,
        Validators.pattern(new RegExp('^[A-Za-z ]+$')),
        Validators.minLength(3),
        Validators.maxLength(15),
      ])],
    idMarca: ['',
      Validators.compose([
        Validators.required,
        Validators.min(1)
      ])],
    idTalla: ['',
    Validators.compose([
      Validators.required,
      Validators.min(1)
    ])],
    quantity: ['',
      Validators.compose([
        Validators.required,
        Validators.pattern(new RegExp('^[0-9]+$')),
        Validators.min(1),
      ])],
    observations: ['',
      Validators.compose([
        Validators.pattern(new RegExp('^[0-9a-zA-Z ]+$')),
        Validators.minLength(1),
        Validators.maxLength(150),
      ])],
    date_shipment: ['',
      Validators.compose([
        Validators.required,
      ])],
  });

  ngOnInit() {
    if (this.content.opc === 'edit') {
      this.btnActive = true;
      const data = this.content.fields[0][0];
      if (data === undefined || data === "") {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        this.router.navigate(['/home', {}]);
        return;
      }

      this.formProducts.get('id')?.setValue(data.id);
      this.formProducts.get('name')?.setValue(data.nombre);
      this.formProducts.get('idMarca')?.setValue(data.idmarca);
      this.formProducts.get('idTalla')?.setValue(data.idtalla);
      this.formProducts.get('quantity')?.setValue(data.cantidad);
      this.formProducts.get('date_shipment')?.setValue(data.fecha_embarque);
      this.formProducts.get('observations')?.setValue(data.observaciones);
    }

    this.getBrands();
    this.getTallas();

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.modalService.dismissAll();
      }
    });
  }

  close(): void {
    this.modalService.dismissAll();
    //window.location.reload();
  }

  getBrands(): void {
    this.sharedService.showLoad();
    this.brandsService.getBrands().pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      } else if (response.status_code === 200) {
        this.listBrads = response.data;
        console.log(this.listBrads)
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  getTallas(): void {
    this.sharedService.showLoad();
    this.productsService.getTallas().pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      } else if (response.status_code === 200) {
        this.listTallas = response.data;
        console.log(this.listTallas)
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  editProduct(): void {
    const dataObj: any = {
      nombre: this.formProducts.get('name')?.value,
      idmarca: this.formProducts.get('idMarca')?.value,
      idtalla: this.formProducts.get('idTalla')?.value,
      cantidad: this.formProducts.get('quantity')?.value,
      fecha_embarque: this.formProducts.get('date_shipment')?.value,
      observaciones: this.formProducts.get('observations')?.value,
    }
    this.sharedService.showLoad();
    this.productsService.editProduct(this.formProducts.get('id')?.value, dataObj).pipe(first()).subscribe((data: object) => {
      const response: any = data;
      this.sharedService.hideLoad();
      if (typeof (response.status_code) === 'undefined' || response.status_code !== 200) {
        Swal.fire({
          text: 'Ha ocurrido un error',
          imageWidth: 100,
          imageHeight: 100,
        });
        return;
      } else if (response.status_code === 200) {
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        }).then((result) => {
          if (result.isConfirmed) {
            window.location.reload();
          }
        });
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
        confirmButtonColor: '#333',
      });
      this.router.navigate(['/home', {}]);
      return;
    })
  }

  saveBrand(): void {
    const dataObj: any = {
      nombre: this.formProducts.get('name')?.value,
      idmarca: this.formProducts.get('idMarca')?.value,
      idtalla: this.formProducts.get('idTalla')?.value,
      cantidad: this.formProducts.get('quantity')?.value,
      fecha_embarque: this.formProducts.get('date_shipment')?.value,
      observaciones: this.formProducts.get('observations')?.value,
    }
    this.sharedService.showLoad();
    this.productsService.saveProduct(dataObj).pipe(first()).subscribe((data: any) => {
      const response: any = data;
      if (typeof (response.status_code) === 'undefined' || response.status_code === 500) {
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        });
        this.router.navigate(['/home', {}]);
        return;
      } else if (response.status_code === 400) {
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        });
        this.router.navigate(['/home', {}]);
        return;
      } else if (response.status_code === 201) {
        this.sharedService.hideLoad();
        Swal.fire({
          text: response.message,
          imageWidth: 100,
          imageHeight: 100,
        }).then((result) => {
          if (result.isConfirmed) {
            window.location.reload();
          }
        });
      }
    }, (error: any) => {
      this.sharedService.hideLoad();
      Swal.fire({
        text: 'Ha ocurrido un error',
        imageWidth: 100,
        imageHeight: 100,
      });
      this.router.navigate(['/home', {}]);
      return;
    });
  }
}
