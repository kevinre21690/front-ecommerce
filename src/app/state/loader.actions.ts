import { createAction } from '@ngrx/store';

export const show = createAction('[Load Component] show');
export const hide = createAction('[Load Component] hide');




