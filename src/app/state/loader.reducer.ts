import { createReducer, on } from '@ngrx/store';
import { show, hide } from './loader.actions';
import { State } from '@core/models/State';

export const initialState: State = {
  load: false
};

const LOAD = createReducer(
  initialState,
  on(show, (state) => ({ ...state, load: true })),
  on(hide, (state) => ({ ...state, load: false })),
);

export function loadReducer(state: any, action: any): any {
  return LOAD(state, action);
}
