import {Component, HostListener, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {SharedService} from '@core/services/shared/shared.service';
import {Response} from '@core/models/response.model';
import {Subject} from 'rxjs';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  response: Response = {
    code: 0,
    msgResponse: '',
    type: '',
    data: [],
    failMessage: ''
  };

  constructor(
    private sharedService: SharedService,
    private router: Router) {
  }

  click(): any {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    }
    ;
  }

  ngOnInit(): void {
  }

}
