import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import {State} from '@core/models/State';
@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.css']
})
export class SpinnerComponent implements OnInit {
  loader$: Observable<State>;
  constructor( private store: Store<{ load: State }>) {
    this.loader$ = store.select('load');
  }

  ngOnInit(): void {
  }

}
